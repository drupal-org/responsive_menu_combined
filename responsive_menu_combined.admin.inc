<?php

/**
 * @file
 * Forms for Responsive Menu Combined admin screens.
 */

/**
 * Settings form for responsive menu combined.
 */
function responsive_menu_combined_admin_form($form, $form_state) {
  $i = 0;
  $form = array();
  $menus = menu_get_menus();
  $responsive_menu_combined_settings = variable_get('responsive_menu_combined_settings');

  // Wrapper for draggable table.
  $form['draggable_responsive_menu_combined'] = array(
    '#prefix' => '<div id="draggable-attributes">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
    '#theme' => 'responsive_menu_combined_table_drag_components',
  );

  // Create row elements.
  foreach ($menus as $key => $value) {
    if (isset($responsive_menu_combined_settings[$key])) {
      $row_values = $responsive_menu_combined_settings[$key];
    }
    $form['draggable_responsive_menu_combined'][$key]['label'] = array(
      '#type' => 'item',
      '#markup' => $value,
    );
    $form['draggable_responsive_menu_combined'][$key]['friendly_name'] = array(
      '#type' => 'textfield',
      '#default_value' => isset($row_values['friendly_name']) ? $row_values['friendly_name'] : $value,
      '#size' => 60,
      '#maxlength' => 128,
    );

    $form['draggable_responsive_menu_combined'][$key]['enabled'] = array(
      '#type' => 'checkbox',
      '#default_value' => isset($row_values['enabled']) ? $row_values['enabled'] : 0,
    );
    $form['draggable_responsive_menu_combined'][$key]['weight'] = array(
      '#type' => 'textfield',
      '#default_value' => isset($row_values['weight']) ? $row_values['weight'] : $i,
      '#size' => 3,
      '#attributes' => array('class' => array('item-row-weight')),
    );
    // Nesting code based on http://cgit.drupalcode.org/examples/tree/tabledrag_example/tabledrag_example_parent_form.inc?id=7.x-1.x
    $form['draggable_responsive_menu_combined'][$key]['id'] = array(
      '#type' => 'textfield',
      '#size' => 10,
      '#default_value' => $key,
      '#disabled' => TRUE,
      // @todo Get this in the right spot (it's on the form element instead of
      // the cell) at least, but preferably figure out why this is needed at
      // all. It should be applied automatically by tabledrag.
      '#attributes' => array('class' => array('item-row-id', 'tabledrag-hide')),
    );
    $form['draggable_responsive_menu_combined'][$key]['pid'] = array(
      '#type' => 'textfield',
      '#size' => 10,
      '#default_value' => isset($row_values['pid']) ? $row_values['pid'] : '',
      '#attributes' => array('class' => array('item-row-pid')),
    );
    $form['draggable_responsive_menu_combined'][$key]['depth'] = array(
      '#type' => 'hidden',
      '#value' => isset($row_values['depth']) ? $row_values['depth'] : '',
      '#attributes' => array('class' => array('item-row-depth')),
    );
    $i++;
  }
  $form['advanced_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['advanced_options']['display_parent_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display parent title'),
    '#description' => t('When you are inside a sub-navigation pane, display the parents title below the back button.'),
    '#default_value' => variable_get('responsive_menu_combined_display_parent_title'),
  );
  $form['help_text'] = array(
    '#markup' => '<div>' . t('Note: Empty menus will be hidden.') . '</div>',
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#submit' => array('responsive_menu_combined_form_submit'),
  );
  return $form;
}

/**
 * Submit handler for the admin form.
 *
 * This function takes cares of saving the the settings to the database.
 */
function responsive_menu_combined_form_submit($form, &$form_state) {
  variable_set('responsive_menu_combined_settings', $form_state['values']['draggable_responsive_menu_combined']);
  variable_set('responsive_menu_combined_display_parent_title', $form_state['values']['display_parent_title']);
}
