/**
 * @file
 * Adds JavaScript for "friendlier" behaviours when JS is available.
 */

(function ($) {
  "use strict";
  Drupal.behaviors.responsiveMenuCombined = {
    attach: function (context, settings) {
      var rmc = $('.rmc-nav');

      // Return early if there is no responsive menu combined on the page.
      if (!rmc.length) {
        return;
      }

      var responsive_nav_check = rmc.find('#rmc-nav__checkbox');
      var responsive_nav_check_label = responsive_nav_check.siblings('label');
      responsive_nav_check_label.unwrap();
      responsive_nav_check.remove();

      // Convert 'label' to 'button'.
      responsive_nav_check = responsive_nav_check_label.wrapInner('<button class="rmc-nav__navigation-button navigation-button"/>').children(0).unwrap();
      responsive_nav_check.attr('aria-expanded', 'false');

      responsive_nav_check.click(function () {
        var shown = $(this).attr('aria-expanded') === 'true';

        // Set the ul class
        // $(this).parent().find('ul.menu').toggleClass('menu-open-view');

        // Set aria-expanded attribute to match state of checkbox.
        $(this).attr('aria-expanded', shown ? 'false' : 'true');

        // Set min-height attribute on menus.
        var min_height;
        if (!shown) {
          min_height = $('.uw-site--inner').innerHeight();
        }
        else {
          min_height = 'unset';
        }
        $(this).siblings('[role=tablist]').css('min-height', min_height);
      });

      // Remove attribute only valid for no-JS version.
      responsive_nav_check.siblings('[role=tablist]').removeAttr('aria-owns');

      // On focus, set @aria-selected FALSE for all, then TRUE for this one.
      var switch_tabs = function () {
        tabs.each(function () {
          $(this).attr('aria-selected', 'false');
          $(this).attr('tabindex', -1);
        });
        $(this).attr('aria-selected', 'true');
        $(this).attr('tabindex', 0);
      };
      // For tabs, remove 'input' elements, make focussable. Set @aria-selected.
      var tabs = rmc.find('.responsive-nav-menu > [role=tab]');
      tabs.each(function (index) {
        $(this).find('input').remove();

        // Convert 'label' to 'div'.
        var label = $(this).find('label');
        label.wrapInner('<div id="' + label.attr('id') + '" class="tab-label"/>').children(0).unwrap();

        $(this).focus(switch_tabs);
        $(this).data('index', index);
        $(this).keydown(function (event) {
          switch (event.keyCode) {
            // Left.
            case 37:
            // Right.
            case 39:
              var index = $(this).data('index');
              // Distinguish left and right arrow.
              if (event.keyCode === 37) {
                index--;
              }
              else {
                index++;
              }
              // Loop around from the ends.
              if (index >= tabs.length) {
                index = 0;
              }
              if (index < 0) {
                index = tabs.length - 1;
              }
              // Focus the next tab.
              tabs.eq(index).focus();
              break;
          }
        });
      });
      tabs.first().each(switch_tabs);

      // Keep tabbing inside the menu when it is open. When the user tabs off
      // the end of the menu, put focus back around to the start of the menu.
      // When menu is closed #tabloop does not appear so it never gets focus.
      var tabloop = $('<div id="tabloop"/>');
      rmc.find('.responsive-nav-menu').append(tabloop);
      // Make it focussable.
      tabloop.attr('tabindex', 0);
      // When it gets focus, put focus on responsive_nav_check.
      tabloop.focus(function () {
        responsive_nav_check.focus();
      });

      // Add toggle buttons for each sub-menu.
      var subnav = rmc.find('.responsive-nav-menu ul.sub-nav');
      if (subnav) {
        var toggles = $('<button aria-expanded="false" aria-label="Sub-menu" class="toggle-sub nested-arrow"></button>').insertBefore(subnav);
        toggles
          .attr('aria-controls', subnav.attr('id'));

        // Make toggle buttons show and hide the sub-menus.
        toggles.click(function () {
          // Store current state of this button.
          var expanded = $(this).attr('aria-expanded') == 'true';

          // Hide all menus that are not ancestors of this one.
          // toggles.not($(this).parents().siblings('button')).attr('aria-expanded', false);

          // Toggle this button's property.
          $(this).attr('aria-expanded', !expanded);

          // Set the tree structure of classes.

          // Swap menu class if at top level.
          var $twoParents = $(this).parents().eq(1);
          if ($twoParents.hasClass("menu")) {
            $twoParents.toggleClass("top-level-expanded");
          }
          if ($twoParents.hasClass("sub-nav-view") || $twoParents.hasClass("sub-nav-view-open")) {
            $twoParents.toggleClass("sub-nav-view-open sub-nav-view");
          }

          var $threeParents = $(this).parents().eq(2);
          if ($threeParents.hasClass("sub-nav-parent-view") || $threeParents.hasClass("sub-nav-parent")) {
            $threeParents.toggleClass("sub-nav-parent-view sub-nav-parent");
          }

          $(this).parent().toggleClass("sub-nav-parent-view");
          $(this).next().toggleClass("sub-nav-view-open");

          return false;

        });
      }
    }
  };
}(jQuery));
